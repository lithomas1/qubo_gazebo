#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Wrench
import numpy as np

class DepthSetterNode:
    def __init__(self):
        self.desiredDepth = 0
        self.currDepth = 0
        updateRate = 10

        rospy.init_node("DepthController")
        self.rate = rospy.Rate(updateRate)
        self.desiredDepthSub = rospy.Subscriber("desiredDepth", Float32, self.desiredDepthSub)
        self.currDepthSub = rospy.Subscriber("currDepth", Float32, self.currDepthSub)

        self.thrustPub = rospy.Publisher("/qubo_gazebo/thruster_manager/input", Wrench, queue_size = 1)


        while(not rospy.is_shutdown()):
            zResponse = self.getThrustResponse()
            self.writeThrust(zResponse)
            self.rate.sleep()

    def getThrustResponse(self):
        thrustResponse = 0
        #######################################################

        #Write code here to control z thrust to regulate depth#
        
        #######################################################
        return thrustResponse

    def writeThrust(self, val):
        thrustMsg = Wrench()
        thrustMsg.force.z = val
        self.thrustPub.publish(thrustMsg)

    def currDepthSub(self, data):
        self.currDepth = data.data

    def desiredDepthSub(self, data):
        self.desiredDepth = data.data

if __name__ == "__main__":
    node = DepthSetterNode()
