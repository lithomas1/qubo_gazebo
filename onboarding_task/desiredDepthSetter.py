#!/usr/bin/env python
import cv2
import rospy
from std_msgs.msg import Float32
from sensor_msgs.msg import FluidPressure
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from moviepy.video.io.bindings import mplfig_to_npimage



class DepthSetterNode:
    def __init__(self):
        self.desiredDepth = 0
        self.currDepth = 0
        self.arrIndex = 0
        self.currDepthArray = np.zeros((100))
        self.desiredDepthArray = np.zeros((100))
        self.indexListArray = np.arange(0, 100)
        updateRate = 5

        rospy.init_node("DepthSetter")
        self.rate = rospy.Rate(updateRate)
        self.desiredDepthPub = rospy.Publisher("desiredDepth", Float32, queue_size = 1)
        self.currPressSub = rospy.Subscriber("/qubo_gazebo/pressure", FluidPressure, self.currDepthSub)
        self.currDepthPub = rospy.Publisher("currDepth", Float32, queue_size = 1)


        self.depthSliderMax = 6 #meters
        self.titleWindow = "Depth Controller"
        cv2.namedWindow(self.titleWindow)
        self.trackbar_name = "Desired Depth(m): "
        cv2.createTrackbar(self.trackbar_name, self.titleWindow , 0, self.depthSliderMax, self.trackbarCallback)
        while(cv2.waitKey(33) != ord('q')):
            self.redrawWindow()
            self.rate.sleep()
        cv2.destroyAllWindows()



    def redrawWindow(self):
        self.currDepthArray[self.arrIndex] = self.currDepth
        self.desiredDepthArray[self.arrIndex] = self.desiredDepth

        self.arrIndex += 1
        if self.arrIndex >= 100:
            self.arrIndex -= 100

        graphRGB = self.drawPlot()
        gh, gw, _ = graphRGB.shape
        cv2.imshow(self.titleWindow, graphRGB)
    

    def drawPlot(self):
        fig, ax = plt.subplots(figsize=(4,3), facecolor='w')
        updatedIndex = np.mod(self.indexListArray - self.arrIndex, np.array([100]))
        currLine, = ax.plot(updatedIndex[0:self.arrIndex],self.currDepthArray[0:self.arrIndex], color='red')
        depthLine, = ax.plot(updatedIndex[0:self.arrIndex],self.desiredDepthArray[0:self.arrIndex], color='blue')
        currLine2, = ax.plot(updatedIndex[self.arrIndex:],self.currDepthArray[self.arrIndex:], color='red')
        depthLine2, = ax.plot(updatedIndex[self.arrIndex:],self.desiredDepthArray[self.arrIndex:], color='blue')
        plt.xlim((0,100))
        plt.ylim([0, self.depthSliderMax + 1])  # setup wide enough range here
        plt.box('off')
        plt.tight_layout()
        graphRGB = mplfig_to_npimage(fig)
        plt.close()
        return graphRGB

    def trackbarCallback(self,val):
        self.desiredDepth = val
        self.desiredDepthPub.publish(self.desiredDepth)

    def currDepthSub(self, data):
        pressure = data.fluid_pressure
        atmPressure = 101.325
        kpaPerMeter = 9.78
        depthOffset = -0.24
        self.currDepth = (pressure - atmPressure) / kpaPerMeter
        self.currDepth += depthOffset
        self.currDepthPub.publish(self.currDepth)
        print(self.currDepth)

if __name__ == "__main__":
    node = DepthSetterNode()
